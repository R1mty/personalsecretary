//
//  AddBusinessViewController.swift
//  personalSecretary
//
//  Created by Давид on 17.09.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import UIKit

class AddBusinessViewController: UIViewController {
    
    @IBOutlet var nameTextField: UITextField! {
        didSet {
            nameTextField.placeholder = "Введите название дела"
        }
    }
    @IBOutlet var timeTextField: UITextField! {
        didSet {
            timeTextField.placeholder = "Введите сколько займет дело"
        }
    }
    @IBOutlet var priorityTextField: UITextField! {
        didSet {
            priorityTextField.placeholder = "Выберите срочность"
        }
    }
    @IBOutlet var contactTextField: UITextField! {
        didSet {
            contactTextField.placeholder = "Выберите контакт"
        }
    }
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Добавление Дела"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
}

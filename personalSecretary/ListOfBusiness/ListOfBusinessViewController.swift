//
//  ListOfBusinessViewController.swift
//  personalSecretary
//
//  Created by Давид on 17.09.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import UIKit

class Business {
    var name: String!
    var time: String!
    var priority: String!
    var contact: String!
}

class ListOfBusinessTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Список ваших дел"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfBussinessTableCell", for: indexPath) as! ListOfBussinessTableCell
        
        cell.textLabel?.text = "kjbsfgkjlksdbfg"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("sflkgsl;kbdflkjbsdklfjbglksjbdfgk;lsjbdfkljgvbslkjdbfglkseghs")
    }
    
    @objc func addBus() {
        let add = UIStoryboard(name: "AddBusinessViewController", bundle: nil).instantiateViewController(withIdentifier: "AddBusinessViewController") as! AddBusinessViewController
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    
    lazy var faButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named:"Union"), for: .normal)
        button.addTarget(self, action: #selector(addBus), for: .touchUpInside)
        button.layer.cornerRadius = 30
        return button
     }()
     
    
    
    
    
    
}


extension ListOfBusinessTableViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let view = UIApplication.shared.keyWindow {
            view.addSubview(faButton)
            setupButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let view = UIApplication.shared.keyWindow, faButton.isDescendant(of: view) {
            faButton.removeFromSuperview()
        }
    }
    
    func setupTableView() {
        tableView.backgroundColor = .darkGray
    }

    func setupButton() {
        NSLayoutConstraint.activate([
            faButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            faButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -36),
            faButton.heightAnchor.constraint(equalToConstant: 60),
            faButton.widthAnchor.constraint(equalToConstant: 60)
            ])
    }
}


class ListOfBussinessTableCell: UITableViewCell {
    
    
    
}

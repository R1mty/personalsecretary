//
//  AuthViewController.swift
//  personalSecretary
//
//  Created by Давид on 17.09.2020.
//  Copyright © 2020 Давид. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {
    
    @IBOutlet var loginTextField: UITextField! {
        didSet {
            loginTextField.placeholder = "Логин"
        }
    }
    @IBOutlet var passwordTextField: UITextField! {
           didSet {
               passwordTextField.placeholder = "Пароль"
           }
       }
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet var regButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.addTarget(self, action: #selector(goNext), for: .touchUpInside)
        regButton.addTarget(self, action: #selector(reg), for: .touchUpInside)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func goNext() {
        let list = UINavigationController(rootViewController: UIStoryboard(name: "ListOfBusinessTableView", bundle: nil).instantiateViewController(withIdentifier: "ListOfBusinessTableViewController") as! ListOfBusinessTableViewController)
        list.modalPresentationStyle = .fullScreen
        self.present(list, animated: true, completion: nil)
    }
    
    @objc func reg() {
        
    }
    
}

